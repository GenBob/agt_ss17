import Graph.Node as Node
import Graph.Graph as Graph
import Graph.Edge as Edge

import math
import queue


def check_for_circle1(graph):
	counter = 0
	# if we find enough nodes with >= 2 edges, we can be sure, that there is a circle
	goal = math.ceil(len(graph.list_nodes) / 2) + 1

	for node in graph.list_nodes:
		if len(node.list_edges) >= 2:
			counter += 1
			if counter >= (3 and goal):
				return True

	return False


def check_for_circle2(graph):
	node_queue = queue.Queue()
	node_queue.put(graph.list_nodes[0])  # Create a new queue and put the first node into it
	graph.list_nodes[0].set_visited(True)

	while not node_queue.empty():
		node = node_queue.get()

		for edge in node.list_edges:
			# get other node
			if edge.node_1 == node and edge.node_2 != node:
				other_node = edge.node_2
			elif edge.node_2 == node and edge.node_1 != node:
				other_node = edge.node_1
			else:  # we have a edge, with the same node on both sides
				continue

			if node.get_prev_node() is other_node:  # dont look back <3
				continue

			# check for every edge if the other node is already visited
			# and if the next node has more than 1 edges
			if other_node.get_visited():
				# we found a circle
				return True

			# there is no need to to put nodes with just one edge into the queue
			elif len(other_node.list_edges) > 1:
				other_node.set_prev_node(node)
				node_queue.put(other_node)

			other_node.set_visited(True)

	return False


node1 = Node.ColoredNode(Node.Color.WHITE)
node2 = Node.ColoredNode(Node.Color.WHITE)
node3 = Node.ColoredNode(Node.Color.WHITE)
# node4 = Node.ColoredNode(Node.Color.WHITE)

edge1 = Edge.UndirectedEdge(node1, node1)
node1.list_edges.append(edge1)
#node2.list_edges.append(edge1)

# edge2 = Edge.UndirectedEdge(node1, node4)
edge3 = Edge.UndirectedEdge(node2, node3)
node2.list_edges.append(edge3)
node3.list_edges.append(edge3)

edge4 = Edge.UndirectedEdge(node3, node1)
node1.list_edges.append(edge4)
node3.list_edges.append(edge4)

graph = Graph.Graph([node1, node2, node3], [edge1, edge3, edge4])
result = check_for_circle1(graph)

print(result)
