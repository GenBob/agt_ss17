from enum import Enum


class Node:

    def __init__(self):
        self.list_edges = []
        self.current_position = 0
        self.visited = False
        self.prev_node = None

    def get_next(self):
        self.current_position += 1
        return self.list_edges[self.current_position - 1]

    def get_neighbours(self):
        pass

    def set_visited(self, val):
        self.visited = val

    def get_visited(self):
        return self.visited

    def set_prev_node(self, node):
        self.prev_node = node

    def get_prev_node(self):
        return self.prev_node


class ColoredNode(Node):

    def __init__(self, color):
        Node.__init__(self)
        self.color = color

    def get_next(self):
        while self.current_position < len(self.list_edges):
            if self.list_edges[self.current_position].get_color != (Color.BLACK or Color.GRAY):
                self.current_position += 1
                return self.list_edges[self.current_position - 1]
            else:
                self.current_position += 1

        return None

    def set_color(self, color):
        self.color = color

    def get_color(self):
        return self.color


class Color(Enum):
    WHITE = 0
    GRAY = 1
    BLACK = 2
    RED = 3
    BLUE = 4
