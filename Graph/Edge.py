
class Edge:

    def __init__(self, node_1, node_2):
        self.node_1 = node_1
        self.node_2 = node_2


class UndirectedEdge(Edge):
    def __init__(self, node_1, node_2):
        Edge.__init__(self, node_1, node_2)

    def get_nodes(self):
        return self.node_1, self.node_2
