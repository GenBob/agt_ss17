import Graph.Node as Node
import Graph.Graph as Graph
import Graph.Edge as Edge


def check_two_colored_graph(input_graph):  # 2b
    for edge in input_graph.list_edges:  # loop over all edges ( O(E) )
        n1, n2 = edge.get_nodes()
        if n1.get_color() == n2.get_color():
            return False
    return True


def build_two_colored_graph(input_graph):  # 2c

    color = Node.Color.RED

    for node in input_graph.list_nodes:  # O(V)
        node.set_color(color)  # paint node

        if color == Node.Color.RED:  # switch color
            color = Node.Color.BLUE
        else:
            color = Node.Color.RED

    return check_two_colored_graph(input_graph)  # O(|V| + |E|)


node1 = Node.ColoredNode(Node.Color.WHITE)
node2 = Node.ColoredNode(Node.Color.WHITE)
node3 = Node.ColoredNode(Node.Color.WHITE)
node4 = Node.ColoredNode(Node.Color.WHITE)

edge1 = Edge.UndirectedEdge(node1, node2)
edge2 = Edge.UndirectedEdge(node1, node4)
edge3 = Edge.UndirectedEdge(node2, node3)
edge4 = Edge.UndirectedEdge(node3, node4)

graph = Graph.Graph([node1, node2, node3, node4], [edge1, edge2, edge3, edge4])


print(build_two_colored_graph(graph))




